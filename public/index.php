<?php
require_once './vendor/autoload.php';

use Slim\Http\Request;
use Slim\Http\Response;

const UPLOAD_LOCATION = __DIR__ . '/../data/orders.csv';

// instantiate the App object
$app = new \Slim\App();

$csvPresent = false;

if (file_exists(UPLOAD_LOCATION)) {
    $csvPresent = true;
}

$templates = new League\Plates\Engine('./resources/views');

// Add route callbacks

$app->get('/', function(Request $request, Response $response) use ($templates, $csvPresent) {
    return $response->withStatus(200)->write($templates->render('home', ['csvPresent' => $csvPresent]));
});

$app->post('/', function(Request $request, Response $response) {
    $directory = __DIR__ . '/../data';

    $uploadedFiles = $request->getUploadedFiles();

    // handle single input with single file upload
    $uploadedFile = $uploadedFiles['csvFile'];

    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        if ($extension !== 'csv') {
            return $response->withStatus(500)->write('error: must be csv format');
        }

        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . 'orders.csv');

        return $response->withStatus(302)->withHeader('Location', '/schools');
    }

    return $response->withStatus(500)->write('error');
});

// If we've got a csv then we'll add these routes
if ($csvPresent) {
    // Web routes
    $csv = new \SchoolApp\Iterators\CsvOrderItemIterator(UPLOAD_LOCATION);
    $repo = new \SchoolApp\Repositories\CsvSchoolRepository($csv);

    $app->get('/schools', function(Request $request, Response $response) use ($repo, $templates) {
        $data = [
            'schools' => $repo->getAllSchools(),
        ];

        return $response->withStatus(200)->write($templates->render('schools', $data));
    });


    $app->get('/schools/{urn}', function(Request $request, Response $response) use ($repo, $templates) {
        $route = $request->getAttribute('route');
        $urn = (int) $route->getArgument('urn');

        $data = [
            'school' => $repo->getSchool($urn),
            'orders' => $repo->getOrdersForSchool($urn),
        ];

        return $response->withStatus(200)->write($templates->render('school', $data));
    });

    $app->get('/schools/{urn}/orders/{orderId}', function(Request $request, Response $response) use ($repo, $templates) {
        $route = $request->getAttribute('route');
        $urn = (int) $route->getArgument('urn');
        $orderId = (int) $route->getArgument('orderId');

        $data = [
            'school' => $repo->getSchool($urn),
            'products' => $repo->getProductForOrder($urn, $orderId),
            'order' => $repo->getOrder($urn, $orderId),
        ];

        return $response->withStatus(200)->write($templates->render('order', $data));
    });

    // API Routes
    $app->get('/api/schools', function(Request $request, Response $response) use ($repo) {
        return $response->withStatus(200)->withJson($repo->getAllSchools());
    });

    $app->get('/api/schools/{urn}', function(Request $request, Response $response) use ($repo, $templates) {
        $route = $request->getAttribute('route');
        $urn = (int) $route->getArgument('urn');

        return $response->withStatus(200)->withJson($repo->getSchool($urn));
    });

    $app->get('/api/schools/{urn}/orders', function(Request $request, Response $response) use ($repo, $templates) {
        $route = $request->getAttribute('route');
        $urn = (int) $route->getArgument('urn');

        return $response->withStatus(200)->withJson($repo->getOrdersForSchool($urn));
    });


    $app->get('/api/schools/{urn}/orders/{orderId}', function ($request, $response, $args) use ($repo, $templates) {
        $route = $request->getAttribute('route');
        $urn = (int) $route->getArgument('urn');
        $orderId = (int) $route->getArgument('orderId');

        return $response->withStatus(200)->withJson($repo->getOrder($urn, $orderId));
    });

    $app->get('/api/schools/{urn}/orders/{orderId}/order-items', function ($request, $response, $args) use ($repo, $templates) {
        $route = $request->getAttribute('route');
        $urn = (int) $route->getArgument('urn');
        $orderId = (int) $route->getArgument('orderId');

        return $response->withStatus(200)->withJson($repo->getProductForOrder($urn, $orderId));
    });
}

// Run application
$app->run();
