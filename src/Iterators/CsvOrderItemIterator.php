<?php
namespace SchoolApp\Iterators;

use SchoolApp\Assert;
use SplFileObject;

class CsvOrderItemIterator implements OrderItemIteratorContract
{
    /**
     * @var int
     */
    private $position = 0;

    /**
     * @var \SplFileObject
     */
    private $fileIterator;

    /**
     * @var array
     */
    private $columnPositionMap;


    /**
     * CsvOrderItemIterator constructor.
     *
     * @param string $fileName
     */
    public function __construct(string $fileName)
    {
        $fileIterator = new SplFileObject($fileName);

        $fileIterator->setFlags(
            SplFileObject::READ_CSV
            | SplFileObject::READ_AHEAD
            | SplFileObject::SKIP_EMPTY
            | SplFileObject::DROP_NEW_LINE
        );

        $fileIterator->setCsvControl(',');

        $headers = $fileIterator->current();
        $fileIterator->next();

        $expectedHeaders = [
            'school_URN',
            'organization_name',
            'organization_telephone',
            'organization_email',
            'organization_url',
            'order_id',
            'order_date',
            'order_name',
            'order_contact_name',
            'order_email_address',
            'order_telephone',
            'order_delivery_address_1',
            'order_delivery_address_2',
            'order_delivery_address_3',
            'order_delivery_town',
            'order_delivery_county',
            'order_delivery_postcode',
            'organization_bulk_order_total',
            'product_colour_style_ref',
            'product_name',
            'product_colour_name',
            'product_size_name',
            'product_colour_image_url',
            'product_ean',
            'product_price',
            'product_quantity',
            'product_line_price',
        ];

        Assert::arraysContainSameValues('$expectedHeaders', '$headers', $expectedHeaders, $headers);

        // For performance reasons we've going to do this once and store it as a property of the class rather than on
        // each call to current.
        $this->columnPositionMap = array_flip($headers);

        $this->fileIterator = $fileIterator;

        $this->position = 0;
    }

    public function rewind()
    {
        $this->position = 0;
        $this->fileIterator->rewind();

        // we need to skip past the headers again so we call next
        $this->fileIterator->next();
    }

    /**
     * @return \SchoolApp\Entities\Product
     */
    public function current() : array
    {
        $data = $this->fileIterator->current();

        // We use the map we generated earlier to work out which column each value lives in. This means if the order of
        // the csv changes we don't need to change the code
        return [
            'school' => $this->getCustomerFromRow($data),
            'order' => $this->getOrderFromRow($data),
            'product' => $this->getProductFromRow($data),
        ];
    }

    private function getDataFromRow(array $row, $columnName)
    {
        return $row[$this->columnPositionMap[$columnName]];
    }

    private function getCustomerFromRow($rowData)
    {
        return [
            'schoolUrn' => $this->getDataFromRow($rowData, 'school_URN'),
            'organizationName' => $this->getDataFromRow($rowData, 'organization_name'),
            'organizationTelephone' => $this->getDataFromRow($rowData, 'organization_telephone'),
            'organizationEmail' => $this->getDataFromRow($rowData, 'organization_email'),
            'organizationUrl' => $this->getDataFromRow($rowData, 'organization_url'),
        ];
    }

    private function getOrderFromRow($rowData)
    {
        return [
            'orderId' => $this->getDataFromRow($rowData, 'order_id'),
            'orderDate' => $this->getDataFromRow($rowData, 'order_date'),
            'orderName' => $this->getDataFromRow($rowData, 'order_name'),
            'orderContactName' => $this->getDataFromRow($rowData, 'order_contact_name'),
            'orderEmailAddress' => $this->getDataFromRow($rowData, 'order_email_address'),
            'orderTelephone' => $this->getDataFromRow($rowData, 'order_telephone'),
            'orderDeliveryAddress1' => $this->getDataFromRow($rowData, 'order_delivery_address_1'),
            'orderDeliveryAddress2' => $this->getDataFromRow($rowData, 'order_delivery_address_2'),
            'orderDeliveryAddress3' => $this->getDataFromRow($rowData, 'order_delivery_address_3'),
            'orderDeliveryTown' => $this->getDataFromRow($rowData, 'order_delivery_town'),
            'orderDeliveryCounty' => $this->getDataFromRow($rowData, 'order_delivery_county'),
            'orderDeliveryPostcode' => $this->getDataFromRow($rowData, 'order_delivery_postcode'),
            'organizationBulkOrderTotal' => $this->getDataFromRow($rowData, 'organization_bulk_order_total'),
        ];
    }

    private function getProductFromRow($rowData)
    {
        $productColourImageUrl = trim($this->getDataFromRow($rowData, 'product_colour_image_url'), '//www.ff-ues.com/');

        return [
            'productColourStyleRef' => $this->getDataFromRow($rowData, 'product_colour_style_ref'),
            'productName' => $this->getDataFromRow($rowData, 'product_name'),
            'productColourName' => $this->getDataFromRow($rowData, 'product_colour_name'),
            'productSizeName' => $this->getDataFromRow($rowData, 'product_size_name'),
            'rawProductColourImageUrl' => $this->getDataFromRow($rowData, 'product_colour_image_url'),
            'productColourImageUrl' => $productColourImageUrl,
            'productEan' => $this->getDataFromRow($rowData, 'product_ean'),
            'productPrice' => $this->getDataFromRow($rowData, 'product_price'),
            'productQuantity' => $this->getDataFromRow($rowData, 'product_quantity'),
            'productLinePrice' => $this->getDataFromRow($rowData, 'product_line_price'),
        ];
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * return void
     */
    public function next()
    {
        ++$this->position;
        $this->fileIterator->next();
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return !$this->fileIterator->eof();
    }
}
