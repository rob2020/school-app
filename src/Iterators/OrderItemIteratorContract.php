<?php
namespace SchoolApp\Iterators;

interface OrderItemIteratorContract extends \Iterator
{
    /**
     * @return array
     */
    public function current(): array;
}
