<?php
namespace SchoolApp\Repositories;

use SchoolApp\Iterators\OrderItemIteratorContract;

class CsvSchoolRepository implements SchoolRepositoryContract
{
    /**
     * @var \SchoolApp\Iterators\OrderItemIteratorContract
     */
    private $orderItemIterator;

    /**
     * CsvSchoolRepository constructor.
     *
     * @param \SchoolApp\Iterators\OrderItemIteratorContract $productIterator
     */
    public function __construct(OrderItemIteratorContract $productIterator)
    {
        $this->orderItemIterator = $productIterator;
    }

    /**
     * @return array
     */
    public function getAllSchools(): array
    {
        $results = [];
        foreach ($this->orderItemIterator as $orderItem) {
            $school = $orderItem['school'];
            // We remove deduplicate schools from the results by assigning them to results with the urn. // todo
            $results[$school['schoolUrn']] = $school;
        }

        return $results;
    }

    /**
     * @param int $schoolUrn
     *
     * @return array
     */
    public function getSchool(int $schoolUrn): array
    {
        foreach ($this->orderItemIterator as $orderItem) {
            $school = $orderItem['school'];
            if ($school['schoolUrn'] == $schoolUrn) {
                return $school;
            }
        }

        // If we get to this point then the urn isn't present
        throw new \OutOfBoundsException('school not found');
    }

    /**
     * @param int $schoolUrn
     *
     * @return array
     */
    public function getOrdersForSchool(int $schoolUrn): array
    {
        $results = [];
        foreach ($this->orderItemIterator as $orderItem) {
            $school = $orderItem['school'];
            $order = $orderItem['order'];

            if ($school['schoolUrn'] == $schoolUrn) {
                $results[$order['orderId']] = $order;
            }
        }

        return $results;
    }

    /**
     * @param int $schoolUrn
     * @param int $orderId
     *
     * @return array
     */
    public function getOrder(int $schoolUrn, int $orderId): array
    {
        $results = [];
        foreach ($this->orderItemIterator as $orderItem) {
            $school = $orderItem['school'];
            $order = $orderItem['order'];

            if ($school['schoolUrn'] == $schoolUrn && $order['orderId'] == $orderId) {
                return $order;
            }
        }

        return $results;
    }

    /**
     * @param int $urn
     * @param int $orderId
     *
     * @return array
     */
    public function getProductForOrder(int $urn, int $orderId): array
    {
        $results = [];
        foreach ($this->orderItemIterator as $orderItem) {
            $school = $orderItem['school'];
            $order = $orderItem['order'];
            $product = $orderItem['product'];

            if ($order['orderId'] == $orderId && $school['schoolUrn'] == $urn) {
                $results[] = $product;
            }
        }

        return $results;
    }
}
