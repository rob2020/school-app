<?php
namespace SchoolApp\Repositories;

interface SchoolRepositoryContract
{

    /**
     * @param int $schoolUrn
     * @param int $orderId
     *
     * @return array
     */
    public function getOrder(int $schoolUrn, int $orderId): array;

    /**
     * @param int $urn
     * @param int $orderId
     *
     * @return array
     */
    public function getProductForOrder(int $urn, int $orderId): array;

    /**
     * @param int $schoolUrn
     *
     * @return array
     */
    public function getSchool(int $schoolUrn): array;

    /**
     * @param int $schoolUrn
     *
     * @return array
     */
    public function getOrdersForSchool(int $schoolUrn): array;

    /**
     * @return array
     */
    public function getAllSchools(): array;
}
