<?php

namespace SchoolApp;

use InvalidArgumentException;

class Assert
{
    /**
     * @param string $varNameOne
     * @param string $varNameTwo
     * @param array $a
     * @param array $b
     */
    public static function arraysContainSameValues(string $varNameOne, string $varNameTwo, array $a, array $b)
    {
        asort($a);
        asort($b);

        $a = array_values($a);
        $b = array_values($b);

        if ($a !== $b) {
            throw new InvalidArgumentException(
                sprintf(
                    "$varNameOne and $varNameTwo must contain the same values"
                )
            );
        }
    }
}
