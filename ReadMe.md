### School App

#### Instructions
Linux/mac only.

Should be possible to run on windows but I think the `const UPLOAD_LOCATION = __DIR__ . '/../data/orders.csv';` on line 
7 of index.php would need changing to a windows style path.

Requirements
- php7 or greater
- composer (https://getcomposer.org/)

run `composer install` from the projects root dir.

Depending on you'r setup you may need to change the permissions on the /data dir. `chmod -R 777 data`
from the projects root dir.

run `php -S localhost:8888 -t public public/index.php` from the projects root dir.

navigate to `localhost:8888` in you'r browser.

Upload a csv with the correct format.

#### Architecture 
- All requests to the server are routed to index.php. The Slim framework (www.slimframework.com) then works out which 
route is being called and performances the required action.

- The csv data is parsed by the `CsvOrderItemIterator` then the `CsvSchoolRepository` converts the data 
from `CsvOrderItemIterator` for use in the application and API. The `CsvSchoolRepository` adheres to the 
`SchoolRepositoryContract` interface so in the feature we could implement a `MySqlSchoolRepository` which would again
adhere to the `SchoolRepositoryContract` meaning minimal code changes would be required.

- I've implemented a traditional MVC(ish) application that can be found at `/` and a very basic restful api at `/api`. 
The api isn't powering anything but could be used in the future to power a javascript front end application.

#### Design Decisions
I didn't really have to to fully implement this so I've made a quite a few short cuts that I wouldn't of taken with a 
really app.

- wasn't to sure where the bulk Organization Bulk Order Total should like so added it to order

- The UI is functional but ugly.

- Minimal made efforts with error handling.

- Theirs not really any validation of the csv data apart that is has the correct columns. This could be fixed by either
adding some validation to the CsvOrderItemIterator or creating a object that represents the `schools`, `orders` and 
`orderItems` instead of just passing thing around as a array.

- Everything is parsed to a string and passed throw the app. We should really convert this to a appropeate type. i.e 
DateTime / int / possible some kind of value object.

- Data from the csv isn't being escaped leaves the app vulnerable to xss.

- No Authentication

- The csv is uploaded on the `/` path then all additional paths pull the data directly from the csv using the 
`CsvOrderItemIterator` would be better to upload the csv then add it's data to the a DB then present the data from their.

- Slim is used for the routing as it's keeps things fairly simple. I've opted to use anonymous functions for the routes 
which makes the code a-bit easier to follow but these function could be moved out to controllers to allow the 
application to scale a-bit better in the future.

- I've prevented people accessing the app (`/schools/*`) and the api `/api/*` if a csv hasn't being uploaded. Which felt
abit hacky.

- probable a few other bits I'm forgetting




