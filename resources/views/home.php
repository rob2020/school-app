<?php $this->layout('baseLayout') ?>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Data Uploader</li>
    </ol>
</nav>

<div class="card">
    <div class="card-header"><h5>Upload Data</h5></div>
    <div class="card-body">
        <!-- make sure the attribute enctype is set to multipart/form-data -->
        <form method="post" enctype="multipart/form-data">
            <div class="alert alert-danger" role="alert">
                This will replace any data already uploaded.
            </div>
            <div class="form-group">
                <label for="fileUpload">Upload CSV</label>
                <input name="csvFile" type="file" class="form-control-file" id="fileUpload">
            </div>
            <button type="submit" class="btn btn-primary">Upload</button>
        </form>
    </div>
</div>

<?php if ($csvPresent) : ?>
    <br/>
    <a href="/schools" class="btn btn-success">
        View Results
    </a>
<?php endif ?>
