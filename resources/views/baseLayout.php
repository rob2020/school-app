<html>
<head>
    <title>School Orders</title>
    <link
        rel="stylesheet"
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
        crossorigin="anonymous"
    >
    <style>
        body {
            margin: 2em;
        }
    </style>
</head>
<body>

<?=$this->section('content')?>

</body>
</html>
