<?php $this->layout('baseLayout') ?>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Data Uploader</a></li>
        <li class="breadcrumb-item"><a href="/schools">Schools List</a></li>
        <li class="breadcrumb-item active"><?= $school['organizationName'] ?></li>
    </ol>
</nav>

<div class="card">
    <div class="card-header"><h5 class="card-title">School Details</h5></div>
    <div class="card-body">
        <dl class="row">
            <dt class="col-sm-3">Urn</dt>
            <dd class="col-sm-9"><?= $school['schoolUrn'] ?></dd>

            <dt class="col-sm-3">Name</dt>
            <dd class="col-sm-9"><?= $school['organizationName'] ?></dd>

            <dt class="col-sm-3">Telephone</dt>
            <dd class="col-sm-9"><?= $school['organizationTelephone'] ?></dd>

            <dt class="col-sm-3">Email</dt>
            <dd class="col-sm-9"><?= $school['organizationEmail'] ?></dd>

            <dt class="col-sm-3">Url</dt>
            <dd class="col-sm-9"><?= $school['organizationUrl'] ?></dd>
        </dl>

        <a href="/api/schools/<?= $school['schoolUrn'] ?>">View as API</a>
    </div>
</div>
<br/>
<br/>
<div class="card">
    <div class="card-header"><h5>Orders</h5></div>
    <div class="card-body">
        <table class="table table-striped table-hover">
            <tr>
                <th>View Order</th>
                <th>ID</th>
                <th>Date</th>
                <th>Contact</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Post Code</th>
                <th>Bulk Order Total</th>
            </tr>
            <dt class="col-sm-3">Bulk Order Total</dt>
            <?php foreach ($orders as $order) : ?>
                <tr>
                    <td class="align-middle">
                        <a
                            href="/schools/<?= $school['schoolUrn'] ?>/orders/<?= $order['orderId']; ?>"
                            class="btn btn-success"
                        >
                            View Order
                        </a>
                    </td>
                    <td class="align-middle"><?= $order['orderId']; ?></td>
                    <td class="align-middle"><?= $order['orderDate']; ?></td>
                    <td class="align-middle"><?= $order['orderContactName']; ?></td>
                    <td class="align-middle"><?= $order['orderEmailAddress']; ?></td>
                    <td class="align-middle"><?= $order['orderTelephone']; ?></td>
                    <td class="align-middle"><?= $order['orderDeliveryPostcode']; ?></td>
                    <td class="align-middle">£<?= $order['organizationBulkOrderTotal']; ?></td>
                </tr>
            <?php endforeach;?>
        </table>
        <a href="/api/schools/<?= $school['schoolUrn'] ?>/orders">View as API</a>
    </div>
</div>

