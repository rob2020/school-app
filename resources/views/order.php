<?php $this->layout('baseLayout') ?>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Data Uploader</a></li>
        <li class="breadcrumb-item"><a href="/schools">Schools List</a></li>
        <li class="breadcrumb-item"><a href="/schools/<?= $school['schoolUrn']; ?>"><?= $school['organizationName'] ?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?= $order['orderId']; ?></li>
    </ol>
</nav>

<div class="card">
    <div class="card-header"><h5>Order Details</h5></div>
    <div class="card-body">
        <dl class="row">
            <dt class="col-sm-3">ID</dt>
            <dd class="col-sm-9"><?= $order['orderId']; ?></dd>

            <dt class="col-sm-3">Date</dt>
            <dd class="col-sm-9"><?= $order['orderDate']; ?></dd>

            <dt class="col-sm-3">Contact Name</dt>
            <dd class="col-sm-9"><?= $order['orderContactName']; ?></dd>

            <dt class="col-sm-3">Contact Email</dt>
            <dd class="col-sm-9"><?= $order['orderEmailAddress']; ?></dd>

            <dt class="col-sm-3">Contact Phone Number</dt>
            <dd class="col-sm-9"><?= $order['orderTelephone']; ?></dd>

            <dt class="col-sm-3">Delivery Address</dt>
            <dd class="col-sm-9">
                <?= $order['orderDeliveryAddress1']; ?><br/>
                <?php if ($order['orderDeliveryAddress2']) : ?>
                    <?= $order['orderDeliveryAddress2']; ?><br/>
                <?php endif ?>
                <?php if ($order['orderDeliveryAddress3']) : ?>
                    <?= $order['orderDeliveryAddress3']; ?><br/>
                <?php endif ?>
                <?= $order['orderDeliveryPostcode']; ?>
            </dd>
            <dt class="col-sm-3">Bulk Order Total</dt>
            <dd class="col-sm-9">
                £<?= $order['organizationBulkOrderTotal']; ?>
            </dd>
        </dl>

        <a href="/api/schools/<?= $school['schoolUrn'] ?>/orders/<?= $order['orderId']; ?>">View as API</a>
    </div>
</div>
<br/>
<br/>
<div class="card">
    <div class="card-header"><h5>Order Items</h5></div>
    <div class="card-body">
        <a href="/api/schools/<?= $school['schoolUrn'] ?>/orders/<?= $order['orderId']; ?>/order-items">View as API</a>
        <table class="table table-striped table-hover">
            <tr>
                <th>Colour Image</th>
                <th>Colour Style Ref</th>
                <th>Name</th>
                <th>Colour Name</th>
                <th>Size Name</th>
                <th>Size Name</th>
                <th>Ean</th>
                <th>Price</th>
                <th>Quantity</th>
            </tr>

            <?php foreach ($products as $product) : ?>
                <tr>
                    <td class="align-middle">
                        <img src="<?= $product['productColourImageUrl']; ?>" style="max-width: 150px"/>
                    </td>
                    <td class="align-middle"><?= $product['productColourStyleRef']; ?></td>
                    <td class="align-middle"><?= $product['productName']; ?></td>
                    <td class="align-middle"><?= $product['productColourName']; ?></td>
                    <td class="align-middle"><?= $product['productSizeName']; ?></td>
                    <td class="align-middle"><?= $product['productEan']; ?></td>
                    <td class="align-middle">£<?= $product['productPrice']; ?></td>
                    <td class="align-middle">£<?= $product['productLinePrice']; ?></td>
                    <td class="align-middle"><?= $product['productQuantity']; ?></td>
                </tr>
            <?php endforeach;?>
        </table>
        <a href="/api/schools/<?= $school['schoolUrn'] ?>/orders/<?= $order['orderId']; ?>/order-items">View as API</a>
    </div>
</div>
