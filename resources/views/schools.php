<?php $this->layout('baseLayout') ?>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Data Uploader</a></li>
        <li class="breadcrumb-item active">Schools List</li>
    </ol>
</nav>

<ul class="list-group">
<?php foreach($schools as $school): ?>
    <li class="list-group-item list-group-item-action">
        <a href="/schools/<?= $school['schoolUrn']?>">
            <?= $school['organizationName']?>
        </a>
    </li>
<?php endforeach; ?>
    <br/>
    <a href="/api/schools">View as API</a>
</ul>
